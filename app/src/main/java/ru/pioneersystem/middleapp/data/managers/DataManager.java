package ru.pioneersystem.middleapp.data.managers;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ru.pioneersystem.middleapp.data.network.RestService;
import ru.pioneersystem.middleapp.data.storage.dto.ProductDto;
import ru.pioneersystem.middleapp.App;
import ru.pioneersystem.middleapp.di.DaggerService;
import ru.pioneersystem.middleapp.di.components.DaggerDataManagerComponent;
import ru.pioneersystem.middleapp.di.components.DataManagerComponent;
import ru.pioneersystem.middleapp.di.modules.LocalModule;
import ru.pioneersystem.middleapp.di.modules.NetworkModule;

public class DataManager {
    @Inject
    PreferencesManager mPreferencesManager;

    @Inject
    RestService mRestService;

    private List<ProductDto> mMockProductList;

    public DataManager() {
        DataManagerComponent component = DaggerService.getComponent(DataManagerComponent.class);
        if (component == null) {
            component = DaggerDataManagerComponent.builder()
                    .appComponent(App.getAppComponent())
                    .localModule(new LocalModule())
                    .networkModule(new NetworkModule())
                    .build();
            DaggerService.registerComponent(DataManagerComponent.class, component);
        }
        component.inject(this);

        generateMockData();
    }

    public ProductDto getProductById(int productId) {
        // TODO: 29.10.2016 this temp sample mock data fix me (may be load fron db)
        return mMockProductList.get(productId + 1);
    }

    public void updateProduct(ProductDto product) {
        // TODO: 29.10.2016 update product count or status (something in product) save in db
    }

    public List<ProductDto> getProductList() {
        // TODO: 29.10.2016 load product list from anywhere
        return mMockProductList;
    }

    private void generateMockData() {
        mMockProductList = new ArrayList<>();
        mMockProductList.add(new ProductDto(1, "test 1", "imageUrl", "description 1 description 1 description 1 description 1 description 1", 100, 1));
        mMockProductList.add(new ProductDto(2, "test 2", "imageUrl", "description 1 description 1 description 1 description 1 description 1", 200, 1));
        mMockProductList.add(new ProductDto(3, "test 3", "imageUrl", "description 1 description 1 description 1 description 1 description 1", 300, 1));
        mMockProductList.add(new ProductDto(4, "test 4", "imageUrl", "description 1 description 1 description 1 description 1 description 1", 400, 1));
        mMockProductList.add(new ProductDto(5, "test 5", "imageUrl", "description 1 description 1 description 1 description 1 description 1", 500, 1));
        mMockProductList.add(new ProductDto(6, "test 6", "imageUrl", "description 1 description 1 description 1 description 1 description 1", 600, 1));
        mMockProductList.add(new ProductDto(7, "test 7", "imageUrl", "description 1 description 1 description 1 description 1 description 1", 700, 1));
        mMockProductList.add(new ProductDto(8, "test 8", "imageUrl", "description 1 description 1 description 1 description 1 description 1", 800, 1));
        mMockProductList.add(new ProductDto(9, "test 9", "imageUrl", "description 1 description 1 description 1 description 1 description 1", 900, 1));
        mMockProductList.add(new ProductDto(10, "test 10", "imageUrl", "description 1 description 1 description 1 description 1 description 1", 1000, 1));
    }

    public boolean isAuthUser() {
        // TODO: 29.10.2016 check user auth token in shared preferences
        return true;
    }
}
