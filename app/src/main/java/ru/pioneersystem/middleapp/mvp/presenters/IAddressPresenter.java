package ru.pioneersystem.middleapp.mvp.presenters;

public interface IAddressPresenter {
    void clickOnAddAddress();
}
