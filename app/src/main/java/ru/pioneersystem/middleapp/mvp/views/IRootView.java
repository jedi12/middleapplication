package ru.pioneersystem.middleapp.mvp.views;

import android.support.annotation.Nullable;

public interface IRootView extends IView {
    void showMessage(String message);
    void showError(Throwable t);

    void showLoad();
    void hideLoad();

    @Nullable
    IView getCurrentScreen();
}
