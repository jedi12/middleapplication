package ru.pioneersystem.middleapp.mvp.views;

import ru.pioneersystem.middleapp.data.storage.dto.ProductDto;

public interface IProductView extends IView {
    void showProductView(ProductDto product);
    void updateProductCountView(ProductDto product);
}
