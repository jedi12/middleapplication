package ru.pioneersystem.middleapp.mvp.presenters;

public interface ICatalogPresenter {
    void clickOnByButton(int position);
    boolean checkUserAuth();
}
