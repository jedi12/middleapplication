package ru.pioneersystem.middleapp.mvp.presenters;

public interface IProductPresenter {

    void clickOnPlus();
    void clickOnMinus();
}
