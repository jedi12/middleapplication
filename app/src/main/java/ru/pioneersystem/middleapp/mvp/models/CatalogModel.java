package ru.pioneersystem.middleapp.mvp.models;

import java.util.List;

import ru.pioneersystem.middleapp.data.storage.dto.ProductDto;

public class CatalogModel extends AbstractModel {

    public CatalogModel() {

    }

    public List<ProductDto> getProductList() {
        return mDataManager.getProductList();
    }

    public boolean isUserAurh() {
        return mDataManager.isAuthUser();
    }

    public ProductDto getProductById(int productId) {
        // TODO: 29.10.2016 get product from datamanager
        return mDataManager.getProductById(productId);
    }

    public void updateProduct(ProductDto product) {
        mDataManager.updateProduct(product);
    }
}
