package ru.pioneersystem.middleapp.mvp.views;

import ru.pioneersystem.middleapp.data.storage.dto.UserAddressDto;

public interface IAddressView extends IView {
    void showInputError();
    UserAddressDto getUserAddress();
}
