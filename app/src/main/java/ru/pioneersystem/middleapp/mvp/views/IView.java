package ru.pioneersystem.middleapp.mvp.views;

public interface IView {
    boolean viewOnBackPressed();
}
