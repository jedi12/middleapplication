package ru.pioneersystem.middleapp.mvp.models;

import javax.inject.Inject;

import ru.pioneersystem.middleapp.data.managers.DataManager;
import ru.pioneersystem.middleapp.di.DaggerService;
import ru.pioneersystem.middleapp.di.components.DaggerModelComponent;
import ru.pioneersystem.middleapp.di.components.ModelComponent;
import ru.pioneersystem.middleapp.di.modules.ModelModule;

public abstract class AbstractModel {

    @Inject
    DataManager mDataManager;

    public AbstractModel() {
        ModelComponent component = DaggerService.getComponent(ModelComponent.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(ModelComponent.class, component);
        }
        component.inject(this);
    }

    private ModelComponent createDaggerComponent() {
        return DaggerModelComponent.builder()
                .modelModule(new ModelModule())
                .build();
    }
}
