package ru.pioneersystem.middleapp.mvp.models;

import android.net.Uri;

import java.util.ArrayList;
import java.util.Map;

import ru.pioneersystem.middleapp.data.managers.PreferencesManager;
import ru.pioneersystem.middleapp.data.storage.dto.UserAddressDto;
import ru.pioneersystem.middleapp.data.storage.dto.UserDto;

public class AccountModel extends AbstractModel {

    public UserDto getUserDto() {
        return new UserDto(getUserProfileInfo(), getUserAddress(), getUserSettings());
    }

    private Map<String, String> getUserProfileInfo() {
        return mDataManager.getUserProfileInfo();
    }

    private ArrayList<UserAddressDto> getUserAddress() {
        return mDataManager.getUserAddress();
    }

    private Map<String, Boolean> getUserSettings() {
        return mDataManager.getUserSettings();
    }

    public void saveProfileInfo(String name, String phone) {
        mDataManager.saveProfileInfo(name, phone);
    }

    public void saveAvatarPhoto(Uri photoUri) {
        // TODO: 01.12.2016 implement this
    }

    public void savePromoNotification(boolean isChecked) {
        mDataManager.saveSetting(PreferencesManager.NOTIFICATION_PROMO_KEY, isChecked);
    }

    public void saveOrderNotification(boolean isChecked) {
        mDataManager.saveSetting(PreferencesManager.NOTIFICATION_ORDER_KEY, isChecked);
    }

    public void addAddress(UserAddressDto userAddressDto) {
        mDataManager.addAddress(userAddressDto);
    }

    // TODO: 01.12.2016 remove address
}
