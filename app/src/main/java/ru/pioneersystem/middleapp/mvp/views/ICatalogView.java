package ru.pioneersystem.middleapp.mvp.views;

import java.util.List;

import ru.pioneersystem.middleapp.data.storage.dto.ProductDto;

public interface ICatalogView extends IView {
    void showCatalogView(List<ProductDto> productsList);
    void updateProductCounter();
}
