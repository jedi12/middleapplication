package ru.pioneersystem.middleapp;

import android.app.Application;

import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;
import ru.pioneersystem.middleapp.di.DaggerService;
import ru.pioneersystem.middleapp.di.components.AppComponent;
import ru.pioneersystem.middleapp.di.components.DaggerAppComponent;
import ru.pioneersystem.middleapp.di.modules.AppModule;
import ru.pioneersystem.middleapp.di.modules.PicassoCacheModule;
import ru.pioneersystem.middleapp.di.modules.RootModule;
import ru.pioneersystem.middleapp.mortar.ScreenScoper;
import ru.pioneersystem.middleapp.ui.activities.DaggerRootActivity_Root_Component;
import ru.pioneersystem.middleapp.ui.activities.RootActivity;

public class App extends Application {
    private static AppComponent sAppComponent;
    private MortarScope mRootScope;
    private MortarScope mRootActivityScope;
    private RootActivity.RootComponent mRootActivityRootComponent;

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

    @Override
    public Object getSystemService(String name) {
        return mRootScope.hasService(name) ? mRootScope.getService(name) : super.getSystemService(name);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        createAppComponent();
        createRootActivityComponent();

        mRootScope = MortarScope.buildRootScope()
                .withService(DaggerService.SERVICE_NAME, sAppComponent)
                .build("Root");

        mRootActivityScope = mRootScope.buildChild()
                .withService(DaggerService.SERVICE_NAME, mRootActivityRootComponent)
                .withService(BundleServiceRunner.SERVICE_NAME, new BundleServiceRunner())
                .build(RootActivity.class.getName());

        ScreenScoper.registerScope(mRootScope);
        ScreenScoper.registerScope(mRootActivityScope);
    }

    private void createAppComponent() {
        sAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(getApplicationContext()))
                .build();
    }

    private void createRootActivityComponent() {
        mRootActivityRootComponent = DaggerRootActivity_Root_Component.builder()
                .appComponent(sAppComponent)
                .rootModule(new RootModule())
                .picassoCacheModule(new PicassoCacheModule())
                .build();
    }
}
