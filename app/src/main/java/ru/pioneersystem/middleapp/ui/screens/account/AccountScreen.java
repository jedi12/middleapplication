package ru.pioneersystem.middleapp.ui.screens.account;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;
import mortar.ViewPresenter;
import ru.pioneersystem.middleapp.R;
import ru.pioneersystem.middleapp.di.DaggerService;
import ru.pioneersystem.middleapp.di.scopes.AccountScope;
import ru.pioneersystem.middleapp.flow.AbstractScreen;
import ru.pioneersystem.middleapp.flow.Screen;
import ru.pioneersystem.middleapp.mvp.models.AccountModel;
import ru.pioneersystem.middleapp.mvp.presenters.IAccountPresenter;
import ru.pioneersystem.middleapp.mvp.presenters.RootPresenter;
import ru.pioneersystem.middleapp.mvp.views.IRootView;
import ru.pioneersystem.middleapp.ui.activities.RootActivity;
import ru.pioneersystem.middleapp.ui.screens.address.AddressScreen;

@Screen(R.layout.screen_account)
public class AccountScreen extends AbstractScreen<RootActivity.RootComponent> {
    private int mCustomState = 1;

    public int getCustomState() {
        return mCustomState;
    }

    public void setCustomState(int customState) {
        mCustomState = customState;
    }

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return DaggerAccountScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(new Module())
                .build();
    }

    //region ========================== DI =====================================

    @dagger.Module
    public class Module {
        @Provides
        @AccountScope
        AccountModel provideAccountModel() {
            return new AccountModel();
        }

        @Provides
        @AccountScope
        AccountPresenter provideAccountPresenter() {
            return new AccountPresenter();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @AccountScope
    public interface Component {
        void inject(AccountPresenter presenter);
        void inject(AccountView view);

        RootPresenter getRootPresenter();
        AccountModel getAccountModel();
    }

    //endregion

    //region ========================== Presenter ==============================

    public class AccountPresenter extends ViewPresenter<AccountView> implements IAccountPresenter {
        private Uri mAvatarUri;

        @Inject
        RootPresenter mRootPresenter;

        @Inject
        AccountModel mAccountModel;

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (getView() != null) {
                getView().initView(mAccountModel.getUserDto());
            }
        }

        @Override
        public void clickOnAddress() {
            Flow.get(getView()).set(new AddressScreen());
            // TODO: 01.12.2016 flow open new screen AddressScreen
        }

        @Override
        public void switchViewState() {
            if (getCustomState() == AccountView.EDIT_STATE && getView() != null) {
                mAccountModel.saveProfileInfo(getView().getUserName(), getView().getUserPhone());
                mAccountModel.saveAvatarPhoto(mAvatarUri);
            }

            if (getView() != null) {
                getView().changeState();
            }
        }

        @Override
        public void switchOrder(boolean isChecked) {
            mAccountModel.saveOrderNotification(isChecked);
        }

        @Override
        public void switchPromo(boolean isChecked) {
            mAccountModel.savePromoNotification(isChecked);
        }

        @Override
        public void takePhoto() {
            if (getView() != null) {
                getView().showPhotoSourceDialog();
            }
        }

        @Override
        public void chooseCamera() {
            if (getRootView() != null) {
                getRootView().showMessage("chooseCamera");
            }
            // TODO: 01.12.2016 choose from camera
        }

        @Override
        public void chooseGallery() {
            if (getRootView() != null) {
                getRootView().showMessage("chooseGallery");
            }
            // TODO: 01.12.2016 choose from gallery
        }

        @Nullable
        private IRootView getRootView() {
            return mRootPresenter.getView();
        }
    }

    //endregion
}
