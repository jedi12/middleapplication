package ru.pioneersystem.middleapp.ui.screens.address;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.pioneersystem.middleapp.R;
import ru.pioneersystem.middleapp.data.storage.dto.UserAddressDto;
import ru.pioneersystem.middleapp.di.DaggerService;
import ru.pioneersystem.middleapp.mvp.views.IAddressView;

public class AddressView extends RelativeLayout implements IAddressView {

    @Inject
    AddressScreen.AddressPresenter mPresenter;

    public AddressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            DaggerService.<AddressScreen.Component>getDaggerComponent(context).inject(this);
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
        }
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mPresenter.dropView(this);
        }
    }

    //region ============================= IAddressView ================================

    @Override
    public void showInputError() {
        // TODO: 02.12.2016 implement this
    }

    @Override
    public UserAddressDto getUserAddress() {
        // TODO: 02.12.2016 implement this
        return null;
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    //endregion

    //region ================================= Events ===================================

    @OnClick(R.id.add_address_btn)
    void addAddress() {
        mPresenter.clickOnAddAddress();
    }

    //endregion
}
