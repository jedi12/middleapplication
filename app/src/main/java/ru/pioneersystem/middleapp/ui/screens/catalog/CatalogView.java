package ru.pioneersystem.middleapp.ui.screens.catalog;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.RelativeLayout;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;
import ru.pioneersystem.middleapp.R;
import ru.pioneersystem.middleapp.data.storage.dto.ProductDto;
import ru.pioneersystem.middleapp.di.DaggerService;
import ru.pioneersystem.middleapp.mvp.views.ICatalogView;

public class CatalogView extends RelativeLayout implements ICatalogView {

    @BindView(R.id.add_to_cart_btn) Button addToCartBtn;
    @BindView(R.id.product_pager) ViewPager productPager;
    @BindView(R.id.indicator) CircleIndicator indicator;

    @Inject
    CatalogScreen.CatalogPresenter mPresenter;

    public CatalogView(Context context, AttributeSet attrs) {
        super(context, attrs);

        if (!isInEditMode()) {
            DaggerService.<CatalogScreen.Component>getDaggerComponent(context).inject(this);
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);

        if (!isInEditMode()) {

        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mPresenter.dropView(this);
        }
    }

    @Override
    public void showCatalogView(List<ProductDto> productsList) {
        CatalogAdapter adapter = new CatalogAdapter();
        for (ProductDto product : productsList) {
            adapter.addItem(product);
        }
        productPager.setAdapter(adapter);
        indicator.setViewPager(productPager);
    }

    @Override
    public void updateProductCounter() {
        // TODO: 04.11.2016 update count product on cart icon
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    @OnClick(R.id.add_to_cart_btn)
    void clickAddToCart() {
        mPresenter.clickOnByButton(productPager.getCurrentItem());
    }
}
