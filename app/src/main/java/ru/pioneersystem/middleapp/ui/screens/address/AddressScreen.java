package ru.pioneersystem.middleapp.ui.screens.address;

import javax.inject.Inject;

import dagger.Provides;
import flow.Flow;
import flow.TreeKey;
import mortar.MortarScope;
import mortar.ViewPresenter;
import ru.pioneersystem.middleapp.R;
import ru.pioneersystem.middleapp.di.DaggerService;
import ru.pioneersystem.middleapp.di.scopes.AddressScope;
import ru.pioneersystem.middleapp.flow.AbstractScreen;
import ru.pioneersystem.middleapp.flow.Screen;
import ru.pioneersystem.middleapp.mvp.models.AccountModel;
import ru.pioneersystem.middleapp.mvp.presenters.IAddressPresenter;
import ru.pioneersystem.middleapp.ui.screens.account.AccountScreen;

@Screen(R.layout.screen_add_address)
public class AddressScreen extends AbstractScreen<AccountScreen.Component> implements TreeKey {

    @Override
    public Object getParentKey() {
        return null;
    }

    @Override
    public Object createScreenComponent(AccountScreen.Component parentComponent) {
        return DaggerAddressScreen_Component.builder()
                .component(parentComponent)
                .module(new Module())
                .build();
    }

    //region =============================== DI =====================================

    @dagger.Module
    public class Module {
        @Provides
        @AddressScope
        AddressPresenter provideAddressPresenter() {
            return new AddressPresenter();
        }
    }

    @dagger.Component(dependencies = AccountScreen.Component.class, modules = Module.class)
    @AddressScope
    public interface Component {
        void inject(AddressPresenter presenter);
        void inject(AddressView view);
    }

    //endregion

    //region ============================= Presenter ====================================

    public class AddressPresenter extends ViewPresenter<AddressView> implements IAddressPresenter {

        @Inject
        AccountModel mAccountModel;

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        public void clickOnAddAddress() {
            // TODO: 02.12.2016 save address in model

            if (getView() != null) {
                mAccountModel.addAddress(getView().getUserAddress());
                Flow.get(getView()).goBack();
            }
        }
    }

    //endregion
}
