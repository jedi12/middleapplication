package ru.pioneersystem.middleapp.ui.screens.product;

import android.os.Bundle;

import javax.inject.Inject;

import dagger.Provides;
import mortar.MortarScope;
import mortar.ViewPresenter;
import ru.pioneersystem.middleapp.R;
import ru.pioneersystem.middleapp.data.storage.dto.ProductDto;
import ru.pioneersystem.middleapp.di.DaggerService;
import ru.pioneersystem.middleapp.di.scopes.ProductScope;
import ru.pioneersystem.middleapp.flow.AbstractScreen;
import ru.pioneersystem.middleapp.flow.Screen;
import ru.pioneersystem.middleapp.mvp.models.CatalogModel;
import ru.pioneersystem.middleapp.mvp.presenters.IProductPresenter;
import ru.pioneersystem.middleapp.ui.screens.catalog.CatalogScreen;

@Screen(R.layout.screen_product)
public class ProductScreen extends AbstractScreen<CatalogScreen.Component> {
    private ProductDto mProductDto;

    public ProductScreen(ProductDto product) {
        mProductDto = product;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof ProductScreen && mProductDto.equals(((ProductScreen) o).mProductDto);
    }

    @Override
    public int hashCode() {
        return mProductDto.hashCode();
    }

    @Override
    public Object createScreenComponent(CatalogScreen.Component parentComponent) {
        return DaggerProductScreen_Component.builder()
                .component(parentComponent)
                .module(new Module())
                .build();
    }

    //region ============================ DI ===========================

    @dagger.Module
    public class Module {
        @Provides
        @ProductScope
        ProductPresenter provideProductPresenter() {
            return new ProductPresenter(mProductDto);
        }
    }

    @dagger.Component(dependencies = CatalogScreen.Component.class, modules = Module.class)
    @ProductScope
    public interface Component {
        void inject(ProductPresenter presenter);
        void inject(ProductView view);
    }

    //endregion

    //region ============================ Presenter ===========================

    public class ProductPresenter extends ViewPresenter<ProductView> implements IProductPresenter {

        @Inject
        private CatalogModel mCatalogModel;

        private ProductDto mProduct;

        public ProductPresenter(ProductDto productDto) {
            mProduct = productDto;
        }

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (getView() != null) {
                getView().showProductView(mProduct);
            }
        }

        @Override
        public void clickOnPlus() {
            mProduct.addProduct();
            mCatalogModel.updateProduct(mProduct);
            if (getView() != null) {
                getView().updateProductCountView(mProduct);
            }
        }

        @Override
        public void clickOnMinus() {
            if (mProduct.getCount() > 0) {
                mProduct.deleteProduct();
                mCatalogModel.updateProduct(mProduct);
                if (getView() != null) {
                    getView().updateProductCountView(mProduct);
                }
            }
        }
    }

    //endregion
}
