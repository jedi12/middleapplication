package ru.pioneersystem.middleapp.ui.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import flow.Flow;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;
import ru.pioneersystem.middleapp.BuildConfig;
import ru.pioneersystem.middleapp.R;
import ru.pioneersystem.middleapp.di.DaggerService;
import ru.pioneersystem.middleapp.di.scopes.AuthScope;
import ru.pioneersystem.middleapp.flow.TreeKeyDispatcher;
import ru.pioneersystem.middleapp.mortar.ScreenScoper;
import ru.pioneersystem.middleapp.mvp.presenters.RootPresenter;
import ru.pioneersystem.middleapp.mvp.views.IRootView;
import ru.pioneersystem.middleapp.mvp.views.IView;
import ru.pioneersystem.middleapp.ui.screens.auth.AuthScreen;

import static android.R.attr.typeface;

public class SplashActivity extends AppCompatActivity implements IRootView {
    private ProgressDialog mProgressDialog;

    @BindView(R.id.coordinator_container) CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.app_name_txt) TextView appNameTxt;
    @BindView(R.id.root_frame) FrameLayout mRootFrame;

    @Inject
    RootPresenter mRootPresenter;

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = Flow.configure(newBase, this)
                .defaultKey(new AuthScreen())
                .dispatcher(new TreeKeyDispatcher(this))
                .install();
        super.attachBaseContext(newBase);
    }

    @Override
    public Object getSystemService(String name) {
        MortarScope RootActivityScope = MortarScope.findChild(getApplicationContext(), RootActivity.class.getName());
        return RootActivityScope.hasService(name) ? RootActivityScope.getService(name) : super.getSystemService(name);
    }

    //region =============================== Life cycle ==================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BundleServiceRunner.getBundleServiceRunner(this).onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        DaggerService.<RootActivity.RootComponent> getDaggerComponent(this).inject(this);

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/PTBebasNeueBook.ttf");
        appNameTxt.setTypeface(typeface);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        BundleServiceRunner.getBundleServiceRunner(this).onSaveInstanceState(outState);
    }

    @Override
    protected void onResume() {
        mRootPresenter.takeView(this);
        super.onResume();
    }

    @Override
    protected void onPause() {
        mRootPresenter.dropView();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (isFinishing()) {
            ScreenScoper.destroyScreenScope(AuthScreen.class.getName());
        }
        super.onDestroy();
    }

    //endregion

    //region =============================== IAuthView ===================================

    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable t) {
        if (BuildConfig.DEBUG) {
            showMessage(t.getMessage());
            t.printStackTrace();
        } else {
            showMessage("Что-то пошло не так, попробуйте позже");
            //TODO: send error stacktrace to crashlytics
        }
    }

    @Override
    public void showLoad() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this, R.style.custom_dialog);
            mProgressDialog.setCancelable(false);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mProgressDialog.show();
            mProgressDialog.setContentView(R.layout.progress_circle);
        } else {
            mProgressDialog.show();
            mProgressDialog.setContentView(R.layout.progress_circle);
        }
    }

    @Override
    public void hideLoad() {
        if (mProgressDialog != null) {
            if (mProgressDialog.isShowing()) {
                mProgressDialog.hide();
            }
        }
    }

    @Nullable
    @Override
    public IView getCurrentScreen() {
        return (IView) mRootFrame.getChildAt(0);
    }

    //endregion =================================================================


    @Override
    public void onBackPressed() {
        if (getCurrentScreen() != null && !getCurrentScreen().viewOnBackPressed() && !Flow.get(this).goBack()) {
            super.onBackPressed();
        }
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    public void startRootActivity() {
        Intent intent = new Intent(this, RootActivity.class);
        startActivity(intent);
        finish();
    }
}
