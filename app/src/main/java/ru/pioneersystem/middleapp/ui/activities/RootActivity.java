package ru.pioneersystem.middleapp.ui.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import flow.Flow;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;
import ru.pioneersystem.middleapp.BuildConfig;
import ru.pioneersystem.middleapp.R;
import ru.pioneersystem.middleapp.di.DaggerService;
import ru.pioneersystem.middleapp.di.components.AppComponent;
import ru.pioneersystem.middleapp.di.modules.PicassoCacheModule;
import ru.pioneersystem.middleapp.di.modules.RootModule;
import ru.pioneersystem.middleapp.di.scopes.RootScope;
import ru.pioneersystem.middleapp.flow.TreeKeyDispatcher;
import ru.pioneersystem.middleapp.mvp.presenters.RootPresenter;
import ru.pioneersystem.middleapp.mvp.views.IRootView;
import ru.pioneersystem.middleapp.mvp.views.IView;
import ru.pioneersystem.middleapp.ui.screens.account.AccountScreen;
import ru.pioneersystem.middleapp.ui.screens.auth.AuthScreen;
import ru.pioneersystem.middleapp.ui.screens.catalog.CatalogScreen;

public class RootActivity extends AppCompatActivity implements IRootView, NavigationView.OnNavigationItemSelectedListener {
    private ProgressDialog mProgressDialog;

    @Inject
    RootPresenter mRootPresenter;

    TextView goodsCount;
    int count = 0;

    @BindView(R.id.drawer_layout) DrawerLayout mDrawer;
    @BindView(R.id.nav_view) NavigationView mNavigationView;
    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.coordinator_container) CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.root_frame) FrameLayout mRootFrame;

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = Flow.configure(newBase, this)
                .defaultKey(new CatalogScreen())
                .dispatcher(new TreeKeyDispatcher(this))
                .install();
        super.attachBaseContext(newBase);
    }

    @Override
    public Object getSystemService(String name) {
        MortarScope RootActivityScope = MortarScope.findChild(getApplicationContext(), RootActivity.class.getName());
        return RootActivityScope.hasService(name) ? RootActivityScope.getService(name) : super.getSystemService(name);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);
        BundleServiceRunner.getBundleServiceRunner(this).onCreate(savedInstanceState);
        ButterKnife.bind(this);
        RootComponent rootComponent = DaggerService.getDaggerComponent(this);
        rootComponent.inject(this);

        initToolbar();
        initDrawer();
        mRootPresenter.takeView(this);
        mRootPresenter.initView();
        // TODO: 04.11.2016 init View

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        BundleServiceRunner.getBundleServiceRunner(this).onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        mRootPresenter.dropView();
        super.onDestroy();
    }

    private void initDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawer, mToolbar, R.string.open_drawer, R.string.close_drawer);
        mDrawer.setDrawerListener(toggle);
        toggle.syncState();
        mNavigationView.setNavigationItemSelectedListener(this);
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
    }

    @Override
    public void onBackPressed() {
        if (getCurrentScreen() != null && !getCurrentScreen().viewOnBackPressed() && !Flow.get(this).goBack()) {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        item.setChecked(true);
        Object key = null;

        switch (item.getItemId()) {
            case R.id.nav_account:
                key = new AccountScreen();
                break;
            case R.id.nav_catalog:
                key = new CatalogScreen();
                break;
            case R.id.nav_favorite:
                break;
            case R.id.nav_orders:
                break;
            case R.id.nav_notification:
                break;
        }

        if (key != null) {
            Flow.get(this).set(key);
        }

        mDrawer.closeDrawer(GravityCompat.START);
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.cart_menu, menu);
        MenuItem item = menu.findItem(R.id.cart_menu_item);
        MenuItemCompat.setActionView(item, R.layout.cart_menu_layout);
        FrameLayout frameLayout = (FrameLayout) MenuItemCompat.getActionView(item);

        goodsCount = (TextView) frameLayout.findViewById(R.id.goods_count);
        goodsCount.setText("");

        return true;
    }

    public void addCartCount() {
        count++;
        goodsCount.setText(String.valueOf(count));
    }

    //region =================================== IRootView ====================================

    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable t) {
        if (BuildConfig.DEBUG) {
            showMessage(t.getMessage());
            t.printStackTrace();
        } else {
            showMessage(getString(R.string.error_message));
            //TODO: send error stacktrace to crashlytics
        }
    }

    @Override
    public void showLoad() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this, R.style.custom_dialog);
            mProgressDialog.setCancelable(false);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mProgressDialog.show();
            mProgressDialog.setContentView(R.layout.progress_circle);
        } else {
            mProgressDialog.show();
            mProgressDialog.setContentView(R.layout.progress_circle);
        }
    }

    @Override
    public void hideLoad() {
        if (mProgressDialog != null) {
            if (mProgressDialog.isShowing()) {
                mProgressDialog.hide();
            }
        }
    }

    @Nullable
    @Override
    public IView getCurrentScreen() {
        return (IView) mRootFrame.getChildAt(0);
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    //endregion

    //region ============================== DI ===================================

    @dagger.Component(dependencies = AppComponent.class, modules = {RootModule.class, PicassoCacheModule.class})
    @RootScope
    public interface RootComponent {
        void inject(RootActivity activity);
        void inject(SplashActivity activity);

        RootPresenter getRootPresenter();
        Picasso getPicasso();
    }

    //endregion
}
