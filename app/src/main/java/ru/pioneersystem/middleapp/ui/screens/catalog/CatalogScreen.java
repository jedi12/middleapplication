package ru.pioneersystem.middleapp.ui.screens.catalog;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;
import mortar.ViewPresenter;
import ru.pioneersystem.middleapp.R;
import ru.pioneersystem.middleapp.data.storage.dto.ProductDto;
import ru.pioneersystem.middleapp.di.DaggerService;
import ru.pioneersystem.middleapp.di.scopes.CatalogScope;
import ru.pioneersystem.middleapp.flow.AbstractScreen;
import ru.pioneersystem.middleapp.flow.Screen;
import ru.pioneersystem.middleapp.mvp.models.CatalogModel;
import ru.pioneersystem.middleapp.mvp.presenters.ICatalogPresenter;
import ru.pioneersystem.middleapp.mvp.presenters.RootPresenter;
import ru.pioneersystem.middleapp.mvp.views.IRootView;
import ru.pioneersystem.middleapp.ui.activities.RootActivity;
import ru.pioneersystem.middleapp.ui.screens.auth.AuthScreen;
import ru.pioneersystem.middleapp.ui.screens.product.ProductScreen;

@Screen(R.layout.screen_catalog)
public class CatalogScreen extends AbstractScreen<RootActivity.RootComponent> {

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return DaggerCatalogScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(new Module())
                .build();
    }

    //region ============================== DI ====================================

    @dagger.Module
    public class Module {
        @Provides
        @CatalogScope
        CatalogModel provideCatalogModel() {
            return new CatalogModel();
        }

        @Provides
        @CatalogScope
        CatalogPresenter provideCatalogPresenter() {
            return new CatalogPresenter();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @CatalogScope
    public interface Component {
        void inject(CatalogPresenter presenter);

        void inject(CatalogView view);

        CatalogModel getCatalogModel();

        Picasso getPicasso();
    }

    //endregion

    //region ============================== Presenter ====================================

    public class CatalogPresenter extends ViewPresenter<CatalogView> implements ICatalogPresenter {

        @Inject
        RootPresenter mRootPresenter;

        @Inject
        CatalogModel mCatalogModel;

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);

            if (getView() != null) {
                getView().showCatalogView(mCatalogModel.getProductList());
            }
        }

        @Override
        public void clickOnByButton(int position) {
            if (getView() != null && getRootView() != null) {
                if (checkUserAuth()) {
                    getView().updateProductCounter();
                    getRootView().showMessage("Товар " + mCatalogModel.getProductList().get(position).getProductName()
                            + " успешно добавлен в корзину");
                } else {
                    Flow.get(getView()).set(new AuthScreen());
                }
            }
        }

        @Nullable
        private IRootView getRootView() {
            return mRootPresenter.getView();
        }

        @Override
        public boolean checkUserAuth() {
            return mCatalogModel.isUserAurh();
        }
    }

    //endregion

    public static class Factory {
        public static Context createProductContext(ProductDto product, Context parentContext) {
            MortarScope parentScope = MortarScope.getScope(parentContext);
            MortarScope childScope = null;
            ProductScreen screen = new ProductScreen(product);
            String scopeName = String.format("%s_%d", screen.getScopeName(), product.getId());

            if (parentScope.findChild(scopeName) == null) {
                childScope = parentScope.buildChild()
                        .withService(DaggerService.SERVICE_NAME,
                                screen.createScreenComponent(DaggerService.<CatalogScreen.Component>getDaggerComponent(parentContext)))
                        .build(scopeName);
            } else {
                childScope = parentScope.findChild(scopeName);
            }
            return childScope.createContext(parentContext);
        }
    }
}
