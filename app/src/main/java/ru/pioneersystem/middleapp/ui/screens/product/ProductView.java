package ru.pioneersystem.middleapp.ui.screens.product;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.pioneersystem.middleapp.R;
import ru.pioneersystem.middleapp.data.storage.dto.ProductDto;
import ru.pioneersystem.middleapp.di.DaggerService;
import ru.pioneersystem.middleapp.mvp.views.IProductView;

public class ProductView extends LinearLayout implements IProductView {
    private static final String TAG = "ProductView";

    @BindView(R.id.product_name_txt) TextView productNameTxt;
    @BindView(R.id.product_description_txt) TextView productDescriptionTxt;
    @BindView(R.id.product_image) ImageView productImage;
    @BindView(R.id.product_count_txt) TextView productCountTxt;
    @BindView(R.id.product_price_txt) TextView productPriceTxt;
    @BindView(R.id.plus_btn) ImageButton plusBtn;
    @BindView(R.id.minus_btn) ImageButton minusBtn;

    @Inject
    Picasso mPicasso;

    @Inject
    ProductScreen.ProductPresenter mPresenter;

    public ProductView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            DaggerService.<ProductScreen.Component>getDaggerComponent(context).inject(this);
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mPresenter.dropView(this);
        }
    }

    // region ============================= IProductView ================================

    @Override
    public void showProductView(final ProductDto product) {
        productNameTxt.setText(product.getProductName());
        productDescriptionTxt.setText(product.getDescription());
        productCountTxt.setText(String.valueOf(product.getCount()));
        if (product.getCount() > 0) {
            productPriceTxt.setText(String.valueOf(product.getCount() * product.getPrice() + ".-"));
        } else {
            productPriceTxt.setText(String.valueOf(product.getPrice() + ".-"));
        }

        mPicasso.load(product.getImageUrl())
                .networkPolicy(NetworkPolicy.OFFLINE)
                .fit()
                .centerCrop()
                .into(productImage, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        mPicasso.load(product.getImageUrl())
                                .fit()
                                .centerCrop()
                                .into(productImage);
                    }
                });
    }

    @Override
    public void updateProductCountView(ProductDto product) {
        productCountTxt.setText(String.valueOf(product.getCount()));
        if (product.getCount() > 0) {
            productPriceTxt.setText(String.valueOf(product.getCount() * product.getPrice() + ".-"));
        }
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    // endregion

    //region ================================= Events =================================

    @OnClick(R.id.plus_btn)
    void clickPlus() {
        mPresenter.clickOnPlus();
    }

    @OnClick(R.id.minus_btn)
    void clickMinus() {
        mPresenter.clickOnMinus();
    }

    //endregion
}
