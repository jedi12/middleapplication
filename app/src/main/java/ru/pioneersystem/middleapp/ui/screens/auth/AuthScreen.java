package ru.pioneersystem.middleapp.ui.screens.auth;

import android.os.Bundle;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import dagger.Provides;
import mortar.MortarScope;
import mortar.ViewPresenter;
import ru.pioneersystem.middleapp.di.DaggerService;
import ru.pioneersystem.middleapp.di.scopes.AuthScope;
import ru.pioneersystem.middleapp.flow.AbstractScreen;
import ru.pioneersystem.middleapp.mvp.models.AuthModel;
import ru.pioneersystem.middleapp.mvp.presenters.IAuthPresenter;
import ru.pioneersystem.middleapp.mvp.presenters.RootPresenter;
import ru.pioneersystem.middleapp.mvp.views.IRootView;
import ru.pioneersystem.middleapp.ui.activities.RootActivity;
import ru.pioneersystem.middleapp.ui.activities.SplashActivity;

public class AuthScreen extends AbstractScreen<RootActivity.RootComponent> {

    private int mCustomState = 1;

    public int getCustomState() {
        return mCustomState;
    }

    public void setCustomState(int customState) {
        mCustomState = customState;
    }

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentRootComponent) {
        return DaggerAuthScreen_Component.builder()
                .component(parentRootComponent)
                .module(new Module())
                .build();
    }

    //region ============================== DI ====================================

    @dagger.Module
    public class Module {
        @Provides
        @AuthScope
        AuthPresenter providePresenter() {
            return new AuthPresenter();
        }

        @Provides
        @AuthScope
        AuthModel provideAuthModel() {
            return new AuthModel();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @AuthScope
    public interface Component {
        void inject(AuthPresenter presenter);
        void inject(AuthView view);
    }

    //endregion

    //region ============================== Presenter ====================================

    public class AuthPresenter extends ViewPresenter<AuthView> implements IAuthPresenter {

        @Inject
        AuthModel mAuthModel;

        @Inject
        RootPresenter mRootPresenter;

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);

            if (getView() != null) {
                if (checkUserAuth()) {
                    getView().hideLoginBtn();
                } else {
                    getView().showLoginBtn();
                }
            }
        }

        @Nullable
        private IRootView getRootView() {
            return mRootPresenter.getView();
        }

        @Override
        public void clickOnLogin() {
            if (getView() != null && getRootView() != null) {
                if (getView().isIdle()) {
                    getView().setCustomState(AuthView.LOGIN_STATE);
                } else {
                    //TODO: auth user
                    mAuthModel.loginUser(getView().getUserEmail(), getView().getUserPassword());
                    getRootView().showMessage("request for user auth");
                }
            }
        }

        @Override
        public void clickOnFb() {
            if (getRootView() != null) {
                getRootView().showMessage("clickOnFb");
            }
        }

        @Override
        public void clickOnVk() {
            if (getRootView() != null) {
                getRootView().showMessage("clickOnVk");
            }
        }

        @Override
        public void clickOnTwitter() {
            if (getRootView() != null) {
                getRootView().showMessage("clickOnTwitter");
            }
        }

        @Override
        public void clickOnShowCatalog() {
            if (getView() != null && getRootView() != null) {
                getRootView().showMessage("Показать каталог");

                if (getRootView() instanceof SplashActivity) {
                    ((SplashActivity) getRootView()).startRootActivity();
                } else {
                    // TODO: 30.11.2016 show catalog screen
                }
            }
        }

        @Override
        public boolean checkUserAuth() {
            return mAuthModel.isAuthUser();
        }
    }

    //endregion
}
