package ru.pioneersystem.middleapp.di.components;

import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Component;
import ru.pioneersystem.middleapp.di.modules.PicassoCacheModule;
import ru.pioneersystem.middleapp.di.scopes.RootScope;

@Component(dependencies = AppComponent.class, modules = PicassoCacheModule.class)
@RootScope
public interface PicassoComponent {
    Picasso getPicasso();
}
