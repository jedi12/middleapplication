package ru.pioneersystem.middleapp.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.pioneersystem.middleapp.data.managers.DataManager;

@Module
public class ModelModule {

    @Provides
    @Singleton
    DataManager provideDataManager() {
        return new DataManager();
    }
}
