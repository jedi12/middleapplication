package ru.pioneersystem.middleapp.di.components;

import javax.inject.Singleton;

import dagger.Component;
import ru.pioneersystem.middleapp.data.managers.DataManager;
import ru.pioneersystem.middleapp.di.modules.LocalModule;
import ru.pioneersystem.middleapp.di.modules.NetworkModule;

@Component(dependencies = AppComponent.class, modules = {NetworkModule.class, LocalModule.class})
@Singleton
public interface DataManagerComponent {
    void inject(DataManager dataManager);
}
