package ru.pioneersystem.middleapp.di.modules;

import dagger.Provides;
import ru.pioneersystem.middleapp.di.scopes.RootScope;
import ru.pioneersystem.middleapp.mvp.presenters.RootPresenter;

@dagger.Module
public class RootModule {
    @Provides
    @RootScope
    RootPresenter provideRootPresenter() {
        return new RootPresenter();
    }
}
