package ru.pioneersystem.middleapp.di.components;

import android.content.Context;

import dagger.Component;
import ru.pioneersystem.middleapp.di.modules.AppModule;

@Component(modules = AppModule.class)
public interface AppComponent {
    Context getContext();
}
