package ru.pioneersystem.middleapp.di.components;

import javax.inject.Singleton;

import dagger.Component;
import ru.pioneersystem.middleapp.di.modules.ModelModule;
import ru.pioneersystem.middleapp.mvp.models.AbstractModel;

@Component(modules = ModelModule.class)
@Singleton
public interface ModelComponent {
    void inject(AbstractModel abstractModel);
}
